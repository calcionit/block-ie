Block IE Browser
================
Block all access in any page if IE is used

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist calcionit/yii2-block-ie "*"
```

or add

```
"calcionit/yii2-block-ie": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \calcionit\blockie\AutoloadExample::widget(); ?>```